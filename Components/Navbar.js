import React, { useState } from 'react'
import './Navbar.css'



import {Link} from 'react-router-dom'

const Navbar = () => {
  const[Menu,setMenu]=useState('home')
  return (
    <div className='My-restro'>
      <div className='navbar-main'>
      <nav className="navbar navbar-expand-lg navbar-light bg-light ">
            <div className="navbar-main-brand">Hunger Bells</div>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
              <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse justify-content:end" id="navbarNavAltMarkup">
              <div className="navbar-nav ml-auto">
                <a className="nav-main-link " href="#"><Link to={'/'} style={{textDecoration:"none",color:"black"}}>Home</Link></a>
                <a className="nav-main-link" href="#"><Link to={'/Menu'} style={{textDecoration:"none",color:"black"}}>Menu</Link></a>
                <a className="nav-main-link" href="#"><Link to={'/About'} style={{textDecoration:"none",color:"black"}}>About</Link></a>
                <a className="nav-main-link" href="#"><Link to={'/Contact'}  style={{textDecoration:"none",color:"black"}}>Contact</Link></a>
              </div>
            </div>
            <button className='nav-button'> <Link to={'/login'}  style={{textDecoration:"none",color:"black"}}>Login</Link></button>
            
          </nav>
          </div> 
     
    </div>
    
  )
}

export default Navbar 