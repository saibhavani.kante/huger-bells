import React, { useState } from 'react'
import Footer from '../Components/Footer'
import photo1_img from "../Components/Assets/shakes-1.png"
import photo2_img from "../Components/Assets/shakes-2.png"
import photo3_img from "../Components/Assets/shakes-3.png"
import photo4_img from "../Components/Assets/shakes-4.png"
import photo5_img from "../Components/Assets/shakes-5.png"
import photo6_img from "../Components/Assets/shakes-6.png"
import photo7_img from "../Components/Assets/shakes-7.png"
import photo8_img from "../Components/Assets/shakes-8.png"




import { Card, CardBody, CardText, CardTitle, CardSubtitle, Button } from 'reactstrap'
const Menu = () => {
    const [all_product, setall_product] = useState([
        {
            id: 1,
            name: "slim fit dark brown shirt",
            category: "menu",
            image:photo1_img ,
            new_price: 5000.0,
            old_price: 3000.5,
          },
          {
            id: 2,
            name: "premimum white shirt",
            category: "menu",
            image: photo2_img,
            new_price: 4000,
            old_price: 3000,
          },
          {
            id: 3,
            name: "lavender premimum shirt",
            category: "menu",
            image: photo3_img,
            new_price: 2500,
            old_price: 3400,
          },
          {
            id: 4,
            name: "navy blue velora shirt",
            category: "menu",
            image: photo4_img,
            new_price: 2800,
            old_price: 4000,
          },
          {
            id: 5,
            name: "pink 3d printed tee",
            category: "menu",
            image: photo5_img,
            new_price: 2500,
            old_price: 3000,
          },
          {
            id: 6,
            name: "black party wear tshirt velora",
            category: "menu",
            image: photo6_img,
            new_price: 2000,
            old_price: 2500,
          },
          {
            id: 7,
            name: "olive polo tshirt cardigian",
            category: "menu",
            image: photo7_img,
            new_price: 1500,
            old_price: 2000,
          },
          {
            id: 8,
            name: "half polo lavender tshirt ",
            category: "menu",
            image: photo8_img,
            new_price: 2100,
            old_price: 3000,
          },
          
          
    ]);

    return (
        <div className='d-flex flex-wrap'>
            <div className='sub-class m-3 p-6 d-flex justify-content-between flex-wrap'  >

            
            {all_product.map((item)=>(<div>
                <Card
                    style={{
                        width: '18rem'
                    }}
                >
                    <img
                        alt="Sample"
                        src={item.image}
                    />
                    <CardBody>
                        <CardTitle tag="h5">
                            {item.name} <br/>
                            Price:{item.new_price}
                            
                        </CardTitle>
                        <CardSubtitle
                            className="mb-2 text-muted"
                            tag="h6"
                        >
                            category:{item.category} 
                            
                        </CardSubtitle>
                        <CardText>
                            Some quick example text to build on the card title and make up the bulk of the card‘s content. <br/>
                            
                        </CardText>
                        <Button>
                            Click Here
                        </Button>
                    </CardBody>
                </Card>
            </div>))}

        </div>
        </div>
    )
}

export default Menu
