
import React from 'react';
import './App.css';
import { BrowserRouter,Route,Routes } from 'react-router-dom';
import Navbar from './Components/Navbar';
import Home from './Pages/Home';
import About from './Pages/About';
import Contact from './Pages/Contact';
import Menu from './Pages/Menu';



import LoginSignup from './Pages/LoginSignup';
import Register from './Pages/Register';

function App() {
  return (
    <BrowserRouter>
    <Navbar />
    <Routes> 
        <Route path='/' element={<Home />}/> 
        <Route path='/About' element={<About/>}/>
        <Route path='/Contact' element={<Contact/>}/>
        <Route path='/Menu' element={<Menu/>}/>
        
        
        <Route path='/login' element={<LoginSignup />}/>
        <Route path='/register' element={<Register />}/>
      </Routes>
    </BrowserRouter>
      
  );
}

export default App;
